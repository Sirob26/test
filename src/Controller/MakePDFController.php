<?php

namespace App\Controller;

use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MakePDFController extends AbstractController
{
  protected $snappy;
  protected $data = '{
        "client": "ESF",
        "date": "17/08/2020",
        "localisation": "Grenoble",
        "logo": "img/don_du_sang.jpg",
        "operateurs": {
          "total": 2504,
          "orange": 933,
          "free": 189,
          "sfr": 762,
          "bouygues": 652,
          "legos": 1,
          "nrjmobile": 8,
          "france": 86,
          "virginemobile": 7,
          "lycamobile": 31
        },
        "envoi": {
          "recu": 2217,
          "erreur": 287,
          "stop": 14
        },
        "erreurs": {
          "envoye": 31,
          "gsm": 60,
          "invalide": 94,
          "expire": 92,
          "autre": 10
        },
        "clics": {
          "cliques": 443,
          "sms": 2504,
          "personne ayant clique": 388
        }
      }';

  public function __construct(Pdf $pdf)
  {
    $this->snappy = $pdf;
  }

  /**
   * @Route("/make/pdf", name="make_pdf")
   */
  public function index()
  {
    $pageUrl = $this->generateUrl('pdf', array(), UrlGeneratorInterface::ABSOLUTE_URL);

    return new PdfResponse($this->snappy->getOutput("https://www.codeproject.com/tips/1120045/export-chart-js-charts-as-image"));
  }

  /**
   * @Route("/pdf", name="pdf")
   */
  public function page()
  {
    $rapport = json_decode($this->data);
    return $this->render("pdf.html.twig", ["rapport" => $rapport]);
  }
}
